/**
 * Мэппинг полей с структуры БД к JSON который требуется передать клиенту
 *
 * @param FIELD_MODEL - описание полей модели
 * @param {[*]} dbStruct - массив записей
 * @returns {{createdAt: *, contactId, contract: {date: *, no: *}, name,
 *    id, businessEntity: *, shortName: *, status, updatedAt: *}
 * }
 */
module.exports.toJSON = (FIELD_MODEL, dbStruct) => {
  const oResult = {};

  // выстроим порядок полей результата в соответствии описанием
  FIELD_MODEL.forEach((field) => {
    if (Object.keys(oResult).find((item) => item === field.scope)) return;
    const key = field.scope ? field.scope : field.apiField;
    oResult[key] = null;
  });

  // сначала выберем блоки в виде массивов
  const aArrayScopes = [
    ...new Set(FIELD_MODEL.filter((item) => item.scopeIsArray).map((item) => item.scope)),
  ];
  // обработаем каждый такой блок
  aArrayScopes.forEach((scope) => {
    oResult[scope] = []; // подготовим ключ в объекте, куда будет помещен массив объектов

    // выберем все поля относящиеся к обрабатываемому блоку
    const aScopeFields = FIELD_MODEL.filter((field) => field.scope === scope);
    const bArray = aScopeFields.length === 1;

    // теперь пройдем по всем записям из переданного набора, чтобы собрать массив объектов
    dbStruct.forEach((item) => {
      const object = bArray ? [] : {};
      aScopeFields.forEach((field) => {
        // получаем имя поля в структуре записи
        const recordField = (field.asField ? field.asField : field.apiField).toLowerCase();
        // записываем значение в конвертированную структуру
        if (item[recordField]) {
          if (bArray) {
            oResult[scope].push(item[recordField]);
          } else {
            object[field.apiField] = item[recordField];
          }
        }
      });
      // уберем возможные дубли, в случае если блок из одного поля в виде массива
      if (bArray) oResult[scope] = [...new Set(oResult[scope])];

      // если в объект хоть что-то попало, добавим его в массив
      const objectKeys = Object.keys(object);
      if (objectKeys.length) {
        // если такой объект уже есть то добавлять не надо, поэтому поищем такой объект
        let bExists = false;
        oResult[scope].forEach((scopedObject) => {
          // сравнение объектов конечно лучше не так сделать, но...
          if (JSON.stringify(scopedObject) === JSON.stringify(object)) bExists = true;
        });
        if (!bExists) oResult[scope].push(object);
      }
    });
    oResult[scope] = [...new Set(oResult[scope])];
  });

  // теперь обработаем вложенные блоки в виде простых объектов
  const aSimpleScopes = [...new Set(
    FIELD_MODEL.filter((item) => item.scope && !item.scopeIsArray).map((item) => item.scope),
  )];
  aSimpleScopes.forEach((scope) => {
    const aScopeFields = FIELD_MODEL.filter((field) => field.scope === scope);
    oResult[scope] = {};
    aScopeFields.forEach((field) => {
      const recordField = (field.asField ? field.asField : field.apiField).toLowerCase();
      oResult[scope][field.apiField] = dbStruct[0][recordField];
    });
  });

  // а теперь просто поля вне всех блоков
  const aFields = FIELD_MODEL.filter((field) => !field.scope);
  aFields.forEach((field) => {
    const recordField = (field.asField ? field.asField : field.apiField).toLowerCase();
    oResult[field.apiField] = dbStruct[0][recordField];
  });

  return oResult;
};
