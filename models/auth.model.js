const { pool, SCHEMA_NAME } = require('../services/db');
const logger = require('../services/logger')(module);

/**
 * Проверка на наличие в БД указанного логина и пароля
 *
 * @param {string} userId
 * @param {string} password
 * @returns {Promise<string>}
 */
module.exports.checkUser = async (userId, password) => {
  try {
    const sQuery = `
      select 
          user_id 
      from 
          ${SCHEMA_NAME}.accounts 
      where 
          user_id = upper($1) and password = crypt($2, password)`;

    const oResult = await pool.query(sQuery, [userId, password]);

    return oResult.rows.length === 1 ? oResult.rows[0].user_id : '';
  } catch (e) {
    // Что-то пошло не так... Фиксируем ошибку в лог и возвращаем пустую строку
    logger.error(e);
  }
  return '';
};
