const authModel = require('../auth.model');
const { pool } = require('../../services/db');

describe('test auth controller', () => {
  it('given correct userId and pass', async () => {
    const response = await authModel.checkUser('admin', 'test');
    expect(response).toBe('ADMIN');
  });
  it('given wrong password', async () => {
    const response = await authModel.checkUser('admin', 'wrong_password');
    expect(response).toBe('');
  });
  it('given wrong userId', async () => {
    const response = await authModel.checkUser('wrong', 'test');
    expect(response).toBe('');
  });
  it('given empty userId', async () => {
    const response = await authModel.checkUser('', 'test');
    expect(response).toBe('');
  });
  it('given empty password', async () => {
    const response = await authModel.checkUser('admin', '');
    expect(response).toBe('');
  });
  it('not given password', async () => {
    const response = await authModel.checkUser('admin');
    expect(response).toBe('');
  });
  it('not given userId and password', async () => {
    const response = await authModel.checkUser();
    expect(response).toBe('');
  });

  afterAll(async () => {
    await pool.end();
  });
});
