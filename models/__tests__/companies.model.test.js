const fs = require('fs');
const path = require('path');
const config = require('../../config.json');
const companiesModel = require('../companies.model');

describe('findOne', () => {
  it('get existed company', async () => {
    const oCompany = await companiesModel.findOne(1);
    expect(oCompany).toHaveProperty('id', 1);
    expect(oCompany).toHaveProperty('name', 'ООО Фирма «Перспективные захоронения»');
    expect(oCompany).toHaveProperty('photos');
    expect(Array.isArray(oCompany.photos)).toBeTruthy();
  });

  it('get not existed company', async () => {
    await expect(companiesModel.findOne(-1))
      .rejects
      .toThrow('Company not found');
  });

  it('not correct id should throw Error', async () => {
    await expect(companiesModel.findOne('aaa'))
      .rejects
      .toThrow('Company not found');
  });
});

describe('findAll', () => {
  it('should return 1 or more items', async () => {
    const oResponse = await companiesModel.findAll();
    expect(oResponse).toHaveProperty('items');
    expect(oResponse).toHaveProperty('total');
    expect(oResponse.items.length).toBeGreaterThan(0);
    expect(oResponse).toHaveProperty('pagination');
    expect(oResponse.pagination).toHaveProperty('currentPage', 1);
    expect(oResponse.pagination).toHaveProperty('perPage', config.perPage);
  });
});

describe('image tests', () => {
  const fileName = '0b8fc462dcabf7610a91.png';
  let imageId = 0;

  it('should return new image id', async () => {
    const _fileName = fileName.split('.').slice(0, -1).join('.');
    const imagePath = path.resolve(`public/${fileName}`);
    const thumbPath = path.resolve(`public/${_fileName}_160x160.png`);

    let imageData = await fs.readFileSync(imagePath, { encoding: 'hex' });
    imageData = `\\x${imageData}`;

    let thumbData = await fs.readFileSync(thumbPath, { encoding: 'hex' });
    thumbData = `\\x${thumbData}`;

    imageId = await companiesModel.addImage(1, fileName, imageData, thumbData);
    expect(imageId).toBeGreaterThan(0);
  });

  it('should return created image', async () => {
    let imageData = await companiesModel.getImage(imageId);

    expect(imageData).toHaveProperty('fileName', fileName);
    expect(imageData).toHaveProperty('data');

    imageData = await companiesModel.getImage(imageId, true);

    expect(imageData).toHaveProperty('fileName', fileName);
    expect(imageData).toHaveProperty('data');
  });

  afterAll(async () => {
    await companiesModel.removeImage(imageId);
  });
});
