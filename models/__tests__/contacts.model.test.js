const contactsModel = require('../contacts.model');

describe('get', () => {
  it('get existed contact', async () => {
    const oContact = await contactsModel.get(1);
    expect(oContact).toHaveProperty('id', 1);
    expect(oContact).toHaveProperty('firstname', 'Иван');
  });

  it('get not existed contact', async () => {
    await expect(contactsModel.get(100))
      .rejects
      .toThrow('Contact not found');
  });

  it('not correct id should throw Error', async () => {
    await expect(contactsModel.get('aaa'))
      .rejects
      .toThrow('Contact not found');
  });
});

describe('update', () => {
  const contact = {
    lastname: 'Григорьев',
    firstname: 'Сергей',
    patronymic: 'Петрович',
    phone: '79162165588',
    email: 'grigoriev@funeral.com',
    createdAt: '2020-11-21T08:03:26.589Z',
    updatedAt: '2020-11-23T09:30:00Z',
  };

  it('should return new data if success', async () => {
    const updatedContact = await contactsModel.update(2, contact);
    expect(updatedContact).toHaveProperty('lastname', contact.lastname);

    await contactsModel.update(2, {
      lastname: 'Петров',
      firstname: 'Петр',
      patronymic: 'Петрович',
      phone: '79162165588',
      email: 'petrov@yahoo.com',
      createdAt: '2020-11-21T08:03:26.589Z',
      updatedAt: '2020-11-23T09:30:00Z',
    });
  });

  it('should throw exception', async () => {
    await expect(contactsModel.update('aaa'))
      .rejects
      .toThrow();
  });
});

describe('delete', () => {
  it('should return 0 if try to delete not existed contact', async () => {
    const affectedRows = await contactsModel.del(1000);
    expect(affectedRows).toEqual(0);
  });

  it('should throw exception', async () => {
    await expect(contactsModel.del('aaa'))
      .rejects
      .toThrow();
  });
});
