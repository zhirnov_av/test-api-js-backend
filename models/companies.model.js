const path = require('path');
const logger = require('../services/logger')(module);
const config = require('../config.json');

const {
  pool, SCHEMA_NAME, startTr, commitTr, rollbackTr,
} = require('../services/db');
const { toJSON } = require('./common');

const MAIN_FROM_CLAUSE = `
    ${SCHEMA_NAME}.companies as company
    left outer join ${SCHEMA_NAME}.images as image on image.company_id = company.id
    left outer join (
      select 
        a.company_id,
        b.type
      from 
        ${SCHEMA_NAME}.company_types as a,
        ${SCHEMA_NAME}.company_types_dict as b
      where 
        b.id = a.type_id
    ) as company_types on company_types.company_id = company.id
`;

const COMPANY_FIELDS = [
  {
    apiField: 'id',
    dbField: 'company.id',
    canSortUsage: false,
    canFilterUsage: false,
  },
  {
    apiField: 'contactId',
    dbField: 'company.contact_id',
    canSortUsage: false,
    canFilterUsage: false,
    updateOrCreate: true,
  },
  {
    apiField: 'name',
    dbField: 'company.name',
    canSortUsage: true,
    canFilterUsage: false,
    updateOrCreate: true,
  },
  {
    apiField: 'shortName',
    dbField: 'company.short_name',
    canSortUsage: false,
    canFilterUsage: false,
    updateOrCreate: true,
  },
  {
    apiField: 'businessEntity',
    dbField: 'company.business_entity',
    canSortUsage: false,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'no',
    dbField: 'company.contract_id',
    canSortUsage: false,
    canFilterUsage: false,
    scope: 'contract',
    updateOrCreate: true,
  },
  {
    apiField: 'date',
    dbField: 'company.contract_date',
    canSortUsage: false,
    canFilterUsage: false,
    scope: 'contract',
    updateOrCreate: true,
  },
  {
    apiField: 'type',
    dbField: 'company_types.type',
    canSortUsage: false,
    canFilterUsage: true,
    scope: 'type',
    scopeIsArray: true,
    updateOrCreate: true,
  },
  {
    apiField: 'status',
    dbField: 'company.status',
    canSortUsage: false,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'address',
    dbField: 'company.address',
    canSortUsage: false,
    canFilterUsage: false,
    updateOrCreate: true,
  },
  {
    apiField: 'createdAt',
    dbField: 'company.created_at',
    canSortUsage: true,
    canFilterUsage: false,
  },
  {
    apiField: 'updatedAt',
    dbField: 'company.updated_at',
    canSortUsage: false,
    canFilterUsage: false,
  },
  {
    apiField: 'id',
    dbField: 'image.id',
    asField: 'image_id',
    canSortUsage: false,
    canFilterUsage: false,
    scope: 'photos',
    scopeIsArray: true,
  },
  {
    apiField: 'name',
    dbField: 'image.name',
    asField: 'image_name',
    canSortUsage: false,
    canFilterUsage: false,
    scope: 'photos',
    scopeIsArray: true,
  },
];

module.exports.COMPANY_FIELDS = COMPANY_FIELDS;

/**
 * Получение из БД компании по переданному идентификатору
 *
 * @param {number} id - идентификатор компании
 * @returns {Promise<*>}
 */
module.exports.findOne = async (id) => {
  const aFields = COMPANY_FIELDS.map((item) => `${item.dbField} as ${item.asField ? item.asField : item.apiField}`);
  const sQuery = `
    select
      ${aFields.join(', ')} 
    from
      ${MAIN_FROM_CLAUSE}
    where
      company.id = $1`;

  try {
    const oResponse = await pool.query(sQuery, [id]);
    if (oResponse.rows.length) {
      return toJSON(COMPANY_FIELDS, oResponse.rows);
    }
  } catch (e) {
    logger.error(e);
  }
  throw new Error('Company not found');
};

/**
 * Получение списка компаний с поддержкой сортировки, фильтрации и пагинации
 *
 * @param {[string]} sortParams
 * @param {[string]} filterParams
 * @param {{page: number, perPage: number}} pageParams
 * @returns {Promise<{
 *    total: number,
 *    pagination: {
 *      perPage: number,
 *      totalPages: number,
 *      currentPage: number
 *    },
 *    items: []
 * }>}
 */
module.exports.findAll = async (sortParams = [], filterParams = [], pageParams = {}) => {
  const sortClause = _processSortParams(sortParams);
  const filterData = _processFilterParams(filterParams);

  const whereClause = filterData.length
    ? `where ${filterData.map((filter) => filter.condition).join(' and ')}`
    : '';

  const params = [];
  filterData.forEach((item) => params.push(...item.values));

  const aFields = COMPANY_FIELDS.map((item) => `${item.dbField} as ${item.asField ? item.asField : item.apiField}`);

  const perPage = pageParams.perPage ? pageParams.perPage : config.perPage;
  const page = pageParams.page || 1;
  const offset = pageParams.page ? (pageParams.page - 1) * perPage : 0;

  const db = await startTr();
  let sQuery = `
    insert into temp_id_table(id)
    (
      select distinct
        company.id
      from
        ${MAIN_FROM_CLAUSE}
      ${whereClause}
    )
  `;
  await db.query(sQuery, params);

  sQuery = 'select count(distinct id) as count from temp_id_table';
  const oResult = await db.query(sQuery);
  const companiesCount = oResult.rows[0].count;

  sQuery = `
    select
      ${aFields.join(', ')} 
    from
      ${MAIN_FROM_CLAUSE}
    where
      company.id in (select id from temp_id_table limit ${perPage} offset ${offset})
    ${sortClause}
    `;

  try {
    // const oResponse = await pool.query(sQuery, params);
    const oResponse = await db.query(sQuery);
    await commitTr(db);
    const uniqueIds = [...new Set(oResponse.rows.map((item) => item.id))];

    const aCompanies = [];
    uniqueIds.forEach((id) => {
      const companyRecords = oResponse.rows.filter((item) => item.id === id);
      aCompanies.push(toJSON(COMPANY_FIELDS, companyRecords));
    });

    const pageCount = companiesCount % perPage
      ? Math.floor(companiesCount / perPage) + 1
      : companiesCount / perPage;

    return {
      items: aCompanies,
      total: +companiesCount,
      pagination: {
        perPage: +perPage,
        totalPages: +pageCount,
        currentPage: +page,
      },
    };
  } catch (e) {
    await rollbackTr(db);
    logger.error(e);
  }
  return {
    items: [],
    total: 0,
    pagination: {
      perPage: +perPage,
      currentPage: +page,
      totalPages: 0,
    },
  };
};

/**
 * Создание компании
 *
 * @param {object} company - объект описывающий атрибуты компании
 * @returns {Promise<number>} - идентификатор созданной компании
 */
module.exports.create = async (company) => {
  const aFields = [];
  const values = [];
  const params = [];
  COMPANY_FIELDS.forEach((item) => {
    if (item.dbField.split('.')[0] === 'company') {
      if (item.dbField === 'company.id') return;
      if (!item.updateOrCreate) return;

      aFields.push(item.dbField.split('.')[1]);
      if (item.scope) {
        const value = company[item.scope] ? company[item.scope][item.apiField] : null;
        values.push(value || null);
      } else {
        values.push(company[item.apiField] || null);
      }
      params.push(`$${params.length + 1}`);
    }
  });
  const creationDate = new Date();
  aFields.push('created_at');
  params.push(`$${params.length + 1}`);
  values.push(creationDate);

  aFields.push('updated_at');
  params.push(`$${params.length + 1}`);
  values.push(creationDate);

  const client = await startTr();
  try {
    let sQuery = `insert into ${SCHEMA_NAME}.companies(${aFields.join(', ')}) values(${params.join(', ')})`;
    await client.query(sQuery, values);

    sQuery = `SELECT currval(pg_get_serial_sequence('${SCHEMA_NAME}.companies','id')) as currval`;
    const oResult = await client.query(sQuery);
    const id = +oResult.rows[0].currval;

    if (company.type) await _updateTypes(client, id, company.type);
    await commitTr(client);

    return id;
  } catch (e) {
    await rollbackTr(client);
    logger.error(e);
    throw e;
  }
};

/**
 * Удаление компании из БД
 *
 * @param {number} id - Идентификатор компании
 * @returns {Promise<void>}
 */
module.exports.delete = async (id) => {
  const client = await startTr();
  try {
    let sQuery = `delete from ${SCHEMA_NAME}.company_types where company_id = $1`;
    await client.query(sQuery, [id]);

    sQuery = `delete from ${SCHEMA_NAME}.images where company_id = $1`;
    await client.query(sQuery, [id]);

    sQuery = `delete from ${SCHEMA_NAME}.companies where id = $1`;
    const result = await client.query(sQuery, [id]);

    if (result.rowCount !== 1) throw new Error(`Can't delete company. Id: ${id}`);
    await commitTr(client);
  } catch (e) {
    await rollbackTr(client);
    logger.error(e);
    throw (e);
  }
};

/**
 * Обновление информации о компании в БД
 *
 * @param {number} id - идентификатор компании
 * @param {object} company - объект описывающий атрибуты компании
 * @returns {Promise<number>} - идентификатор компании
 */
module.exports.update = async (id, company) => {
  const aFields = [];
  const values = [];
  const companyKeys = Object.keys(company);

  COMPANY_FIELDS.forEach((item) => {
    if (item.dbField.split('.')[0] === 'company') {
      if (item.dbField === 'company.id') return;
      if (!item.updateOrCreate) return;
      if (!companyKeys.find((key) => key === item.apiField)) return;

      aFields.push(`${item.dbField.split('.')[1]} = $${aFields.length + 1}`);
      if (item.scope) {
        const value = company[item.scope] ? company[item.scope][item.apiField] : null;
        values.push(value || null);
      } else {
        values.push(company[item.apiField] || null);
      }
    }
  });
  aFields.push(`updated_at = $${aFields.length + 1}`);
  values.push(new Date());

  const client = await startTr();
  try {
    const sQuery = `
      update 
        ${SCHEMA_NAME}.companies 
      set 
        ${aFields.join(', ')}
      where
        id = $${aFields.length + 1}`;
    values.push(id);
    await client.query(sQuery, values);

    await client.query(`delete from ${SCHEMA_NAME}.company_types where company_id = $1`, [id]);

    if (company.type) await _updateTypes(client, id, company.type);
    await commitTr(client);

    return id;
  } catch (e) {
    await rollbackTr(client);
    logger.error(e);
    throw e;
  }
};

/**
 * Добавление изображения к компании
 *
 * @param {number} companyId - идентификатор компании
 * @param {string} name - наименование файла
 * @param {string|Buffer} imageData - содержимое файла
 * @param {string|Buffer} thumbData - содержимое эскиза файла
 *
 * @returns {Promise<number>}
 */
module.exports.addImage = async (companyId, name, imageData, thumbData) => {
  let sQuery = `
    insert into 
        ${SCHEMA_NAME}.images(company_id, name, image_data, thumb_data) 
        values($1, $2, $3, $4)`;
  await pool.query(sQuery, [companyId, name, imageData, thumbData]);
  sQuery = `SELECT currval(pg_get_serial_sequence('${SCHEMA_NAME}.images','id')) as currval`;
  const oResult = await pool.query(sQuery);
  return +oResult.rows[0].currval;
};

/**
 * Получение или эскиза или самого изображения
 *
 * @param {number} imageId - идентификатор изображения
 * @param {boolean} [bThumb] - если истина, то получим эскиз, иначе само изображение
 *
 * @returns {Promise<{fileName, extension: string, data: *}>}
 */
module.exports.getImage = async (imageId, bThumb = false) => {
  const sField = bThumb ? 'thumb_data' : 'image_data';
  const sQuery = `select name, ${sField} from ${SCHEMA_NAME}.images where id = $1`;
  const oResult = await pool.query(sQuery, [imageId]);
  if (oResult.rows.length === 0) throw new Error('Image not found');
  return {
    fileName: oResult.rows[0].name,
    extension: path.extname(oResult.rows[0].name).toLowerCase(),
    data: oResult.rows[0][sField],
  };
};

/**
 * Удаление изображения компании
 *
 * @param id - идентификатор изображения
 *
 * @returns {Promise<void>}
 */
module.exports.removeImage = async (id) => {
  const sQuery = `delete from ${SCHEMA_NAME}.images where id = $1`;
  await pool.query(sQuery, [id]);
};

/**
 * Получения идентификатора компании по идентификатору контакта
 *
 * @param {number} contactId - идентификатор контакта
 *
 * @returns {Promise<*|number>} - идентификатор первой попавшейся компании
 */
module.exports.getCompanyIdByContactId = async (contactId) => {
  const sQuery = `select id from ${SCHEMA_NAME}.companies where contact_id = $1`;
  const result = await pool.query(sQuery, [contactId]);

  return result.rows.length ? result.rows[0].id : 0;
};

/**
 * Метод проверки корректности массива типов компаний
 *
 * @param {[string]} types - массив типов
 *
 * @returns {Promise<boolean>}
 */
module.exports.checkCompanyTypes = async (types) => {
  const params = [];
  const values = [];
  types.forEach((type, index) => {
    params.push(`$${index + 1}`);
    values.push(type);
  });
  const sQuery = `select distinct id from ${SCHEMA_NAME}.company_types_dict where type in (${params.join(',')})`;
  const result = await pool.query(sQuery, values);
  return result.rows.length === types.length;
};

function _processFilterParams(filterParams) {
  let globalIndex = 0;
  return filterParams.map((filter) => {
    const aGroups = /^([a-zA-Z]+)\(([a-zA-Z0-9А-Яа-я,]+)\)$/.exec(filter);
    const apiField = aGroups[1].toUpperCase();
    const aValues = aGroups[2].split(',');

    const { dbField } = COMPANY_FIELDS
      .find((field) => field.canFilterUsage && field.apiField.toUpperCase() === apiField);

    return {
      condition: `${dbField} in ($${aValues.map(() => {
        globalIndex += 1;
        return globalIndex;
      }).join(', $')})`,
      values: aValues,
    };
  });
}

function _processSortParams(sortParams) {
  sortParams = sortParams.map((item) => {
    const apiField = item.split('.')[0].toUpperCase();
    const direction = item.split('.')[1];
    const { dbField } = COMPANY_FIELDS
      .find((field) => field.canSortUsage && field.apiField.toUpperCase() === apiField);

    return `${dbField} ${!direction ? '' : direction}`;
  });
  return sortParams.length ? `order by ${sortParams.join(', ')}` : '';
}

async function _updateTypes(dbClient, companyId, types) {
  const promises = [];
  types.forEach((type) => {
    const sQuery = `
    insert into ${SCHEMA_NAME}.company_types(company_id, type_id) 
      values($1, (select id from ${SCHEMA_NAME}.company_types_dict where type = $2))
  `;
    promises.push(dbClient.query(sQuery, [companyId, type]));
  });
  await Promise.all(promises);
}
