const logger = require('../services/logger')(module);
const { pool, SCHEMA_NAME } = require('../services/db');
const { toJSON } = require('./common');

const CONTACT_FIELDS = [
  {
    apiField: 'id',
    dbField: 'contact.id',
    canSortUsage: false,
    canFilterUsage: false,
  },
  {
    apiField: 'firstname',
    dbField: 'contact.firstname',
    canSortUsage: true,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'lastname',
    dbField: 'contact.lastname',
    canSortUsage: true,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'patronymic',
    dbField: 'contact.patronymic',
    canSortUsage: true,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'phone',
    dbField: 'contact.phone',
    canSortUsage: true,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'email',
    dbField: 'contact.email',
    canSortUsage: true,
    canFilterUsage: true,
    updateOrCreate: true,
  },
  {
    apiField: 'createdAt',
    dbField: 'contact.created_at',
    canSortUsage: true,
    canFilterUsage: false,
  },
  {
    apiField: 'updatedAt',
    dbField: 'contact.updated_at',
    canSortUsage: false,
    canFilterUsage: false,
  },
];

module.exports = {
  CONTACT_FIELDS,
  get,
  update,
  del,
  create,
};

/**
 * Получение из БД контакта по переданному идентификатору
 *
 * @param {number} id - идентификатор компании
 * @returns {Promise<*>}
 */
async function get(id) {
  const aFields = CONTACT_FIELDS.map((item) => `${item.dbField} as ${item.asField ? item.asField : item.apiField}`);
  const sQuery = `
    select
      ${aFields.join(', ')} 
    from
      ${SCHEMA_NAME}.contacts as contact
    where
      contact.id = $1`;

  try {
    const oResponse = await pool.query(sQuery, [id]);
    return toJSON(CONTACT_FIELDS, oResponse.rows);
  } catch (e) {
    logger.error(e);
  }
  throw new Error('Contact not found');
}

/**
 * Обновление контакта по переданному идентификатору
 *
 * @param {number} id - идентификатор компании
 * @returns {Promise<*>}
 */
async function update(id, contact) {
  const aFields = [];
  const values = [];
  const contactKeys = Object.keys(contact);

  CONTACT_FIELDS.forEach((item) => {
    if (item.dbField.split('.')[0] === 'contact') {
      if (item.dbField === 'contact.id') return;
      if (!item.updateOrCreate) return;
      if (!contactKeys.find((key) => key === item.apiField)) return;

      aFields.push(`${item.dbField.split('.')[1]} = $${aFields.length + 1}`);
      if (item.scope) {
        values.push(contact[item.scope][item.apiField]);
      } else {
        values.push(contact[item.apiField]);
      }
    }
  });
  aFields.push(`updated_at = $${aFields.length + 1}`);
  values.push(new Date());

  try {
    const sQuery = `
      update 
        ${SCHEMA_NAME}.contacts 
      set 
        ${aFields.join(', ')}
      where
        id = $${aFields.length + 1}`;
    values.push(id);
    await pool.query(sQuery, values);

    return await get(id);
  } catch (e) {
    logger.error(e);
    throw e;
  }
}

/**
 * Создание контакта по переданному идентификатору
 *
 * @returns {Promise<*>}
 */
async function create(contact) {
  const aFields = [];
  const values = [];
  const params = [];
  CONTACT_FIELDS.forEach((item) => {
    if (item.dbField.split('.')[0] === 'contact') {
      if (item.dbField === 'contact.id') return;
      if (!item.updateOrCreate) return;

      aFields.push(item.dbField.split('.')[1]);
      if (item.scope) {
        const value = contact[item.scope] ? contact[item.scope][item.apiField] : null;
        values.push(value || null);
      } else {
        values.push(contact[item.apiField] || null);
      }
      params.push(`$${params.length + 1}`);
    }
  });
  const creationDate = new Date();
  aFields.push('created_at');
  params.push(`$${params.length + 1}`);
  values.push(creationDate);

  aFields.push('updated_at');
  params.push(`$${params.length + 1}`);
  values.push(creationDate);

  try {
    let sQuery = `insert into ${SCHEMA_NAME}.contacts(${aFields.join(', ')}) values(${params.join(', ')})`;
    await pool.query(sQuery, values);

    sQuery = `SELECT currval(pg_get_serial_sequence('${SCHEMA_NAME}.contacts','id')) as currval`;
    const oResult = await pool.query(sQuery);
    return +oResult.rows[0].currval;
  } catch (e) {
    logger.error(e);
    throw e;
  }
}

async function del(id) {
  try {
    const sQuery = `delete from ${SCHEMA_NAME}.contacts where id = $1`;
    const result = await pool.query(sQuery, [id]);
    return result.rowCount;
  } catch (e) {
    logger.error(e);
    throw e;
  }
}
