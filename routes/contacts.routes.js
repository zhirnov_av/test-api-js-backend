const express = require('express');

const router = express.Router();

const auth = require('../middleware/auth.middleware');
const contactsController = require('../controllers/contacts.controller');
const contactsParamsValidator = require('../middleware/validators/contacts.param.validator');

router.get(
  '/:id',
  auth,
  contactsController.get,
);

router.post(
  '/',
  auth,
  contactsParamsValidator.create,
  contactsController.create,
);

router.patch(
  '/:id',
  auth,
  contactsParamsValidator.update,
  contactsController.update,
);

router.delete(
  '/:id',
  auth,
  contactsParamsValidator.deleteContact,
  contactsController.del,
);

module.exports = router;
