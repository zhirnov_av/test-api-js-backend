const express = require('express');
const multer = require('multer');
const config = require('../config.json');

const fileHandler = multer({ dest: config.uploads_dir });
const router = express.Router();

const auth = require('../middleware/auth.middleware');
const companiesController = require('../controllers/companies.controller');

const filesParamsValidator = require('../middleware/validators/files.params.validator');
const filesController = require('../controllers/files.controller');
const companiesParamsValidator = require('../middleware/validators/companies.param.validator');

router.use(auth);

router.get('/:id', companiesController.get);
router.get('/', companiesParamsValidator.getAllCompanies, companiesController.getAll);
router.post('/', companiesParamsValidator.create, companiesController.create);
router.patch('/:id', companiesParamsValidator.update, companiesController.update);
router.delete('/:id', companiesParamsValidator.del, companiesController.del);

router.post('/:id/images',
  fileHandler.fields([{ name: 'file', maxCount: 1 }]),
  filesParamsValidator.addCompanyImage,
  filesController.saveImage);

router.get('/:id/images/:imageId/image', filesController.getImage);
router.get('/:id/images/:imageId/thumb', filesController.getThumb);
router.delete('/:companyId/images/:imageId', filesParamsValidator.removeCompanyImage, filesController.removeImage);

module.exports = router;
