const request = require('supertest');
const app = require('../../app');

describe('companies tests', () => {
  let token = '';
  beforeAll(async () => {
    const res = await request(app)
      .post('/auth')
      .send({
        user: 'admin',
        password: 'test',
      });
    const tmp = res.headers.authorization.split(' ')[1];
    token = tmp;
  });

  describe('get /companies/{id}', () => {
    it('should return company data', async () => {
      const res = await request(app)
        .get('/companies/1')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('id', 1);
      expect(res.body).toHaveProperty('shortName', 'Перспективные захоронения');
    });

    it('should not found company', async () => {
      const res = await request(app)
        .get('/companies/500')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('error', 'Company not found');
    });
  });

  describe('delete /companies/{id}', () => {
    it('should return error', async () => {
      const res = await request(app)
        .delete('/companies/500')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'Company not found');
    });
  });

  describe('patch /companies/{id}', () => {
    it('should return error', async () => {
      const res = await request(app)
        .patch('/companies/500')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'Company not found');
    });

    it('should not update company because wrong parameters in body', async () => {
      const res = await request(app)
        .patch('/companies/1')
        .send({
          id: 100,
          param: 2000,
        })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'No parameters passed for company update');
    });

    it('should not update company because empty wrong parameters in body', async () => {
      const res = await request(app)
        .patch('/companies/1')
        .send({})
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'No parameters passed for company update');
    });

    it('should update company shortName', async () => {
      const res = await request(app)
        .patch('/companies/1')
        .send({ shortName: 'TEST', contactId: 2 })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(200);
      expect(res.body)
        .toHaveProperty('shortName', 'TEST');
    });

    it('should not update company because given object has wrong parameter', async () => {
      const res = await request(app)
        .patch('/companies/1')
        .send({ shortName: 'TEST', param: 1000 })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Incorrect field (param) in parameters for update company');
    });
  });

  describe('post /companies', () => {
    it('should create company and delete it', async () => {
      const company = {
        contactId: 4,
        name: 'ПАО TEST',
        shortName: 'TEST',
        businessEntity: 'ПАО',
        contract: {
          no: '1111-5555',
          date: '2021-04-30T19:00:00.000Z',
        },
        type: [
          'contractor',
        ],
        status: 'active',
        address: 'г. Тюмень, ул. Пушкина, 12',
        createdAt: '2021-05-16T19:00:00.000Z',
        updatedAt: '2021-05-16T19:00:00.000Z',
      };
      let res = await request(app)
        .post('/companies')
        .send(company)
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('id');
      expect(res.body).toHaveProperty('name', company.name);
      expect(res.body).toHaveProperty('shortName', company.shortName);

      const companyId = res.body.id;

      res = await request(app)
        .delete(`/companies/${companyId}`)
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);
    });

    it('should get error while creating company', async () => {
      const company = {
        contactId: 4,
        name: 'ПАО TEST',
        shortName: 'TEST',
        businessEntity: 'ПАО',
        contract: {
          no: '1111-5555',
          date: '2021-04-30T19:00:00.000Z',
        },
        type: [
          'contractor',
          'test',
        ],
        status: 'active',
        address: 'г. Тюмень, ул. Пушкина, 12',
        createdAt: '2021-05-16T19:00:00.000Z',
        updatedAt: '2021-05-16T19:00:00.000Z',
      };
      const res = await request(app)
        .post('/companies')
        .send(company)
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'Given company\'s types are not correct');
    });
  });

  describe('get /companies', () => {
    it('should get error because perPage parameter are incorrect', async () => {
      const res = await request(app)
        .get('/companies?perPage=a')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'perPage parameter must be greater then or equal to 0');
    });

    it('should get error because page parameter are incorrect', async () => {
      const res = await request(app)
        .get('/companies?page=a')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'page parameter must be greater then or equal to 0');
    });

    it('should return company list with 1 company per page', async () => {
      // получим полный список компаний
      let res = await request(app)
        .get('/companies')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('total');

      const companiesCount = res.body.total;

      // получим полный список компаний c 1й компанией на страницу
      res = await request(app)
        .get('/companies?perPage=1')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('total', companiesCount);
      expect(res.body).toHaveProperty('pagination');
      expect(res.body.pagination).toHaveProperty('totalPages', companiesCount);
      expect(res.body.pagination).toHaveProperty('perPage', 1);
    });

    it('should get sorted companies list', async () => {
      const res = await request(app)
        .get('/companies?sort=name.desc&sort=createdAt')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('items');
      expect(Array.isArray(res.body.items)).toBeTruthy();
      expect(res.body.items.length).toBeGreaterThan(0);
      const item = res.body.items[0];
      expect(item).toHaveProperty('id', 3);
    });

    it('should get error because wrong sort params', async () => {
      const res = await request(app)
        .get('/companies?sort=name1.desc&sort=createdAt-desc')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'Not correct sort params');
    });

    it('should get error because wrong filter params', async () => {
      const res = await request(app)
        .get('/companies?filter=type()')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'Not correct filter params');
    });

    it('should get filtered companies list', async () => {
      const res = await request(app)
        .get('/companies?filter=type(agent,contractor)')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('items');
      expect(Array.isArray(res.body.items)).toBeTruthy();
      expect(res.body.items.length).toBeGreaterThan(0);
      expect(res.body).toHaveProperty('total', 2);
    });
  });

  afterAll(async () => {
    app.close();
  });
});
