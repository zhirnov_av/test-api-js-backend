const request = require('supertest');
const app = require('../../app');

describe('auth tests', () => {
  describe('POST /auth', () => {
    it('should return token in a header', async () => {
      const res = await request(app)
        .post('/auth')
        .send({
          user: 'admin',
          password: 'test',
        });

      expect(res.statusCode).toEqual(200);
      expect(res).toHaveProperty('headers');
      expect(res.headers).toHaveProperty('authorization');
    });

    it('should return error because user param not passed', async () => {
      const res = await request(app)
        .post('/auth')
        .send({
          password: 'test',
        });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'No user passed');
    });

    it('should return error because wrong password was passed', async () => {
      const res = await request(app)
        .post('/auth')
        .send({
          user: 'admin',
          password: 'test2',
        });

      expect(res.statusCode).toEqual(401);
      expect(res.body).toHaveProperty('error', 'User not found or incorrect password');
    });
  });

  afterAll(async () => {
    app.close();
  });
});
