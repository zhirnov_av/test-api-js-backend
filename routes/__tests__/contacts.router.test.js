const request = require('supertest');
const app = require('../../app');

describe('contact routes', () => {
  let token = '';

  beforeAll(async () => {
    const res = await request(app)
      .post('/auth')
      .send({
        user: 'admin',
        password: 'test',
      });
    const tmp = res.headers.authorization.split(' ')[1];
    token = tmp;
  });

  describe('POST /contacts', () => {
    it('should create new contact', async () => {
      const contact = {
        firstname: 'Сергей',
        lastname: 'Иванов',
        patronymic: 'Федорович',
        phone: '79324001950',
        email: 'serik@gmail.com',
      };
      const res = await request(app)
        .post('/contacts')
        .send(contact)
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('firstname', 'Сергей');
    });

    it('should not update contact because wrong parameters in body', async () => {
      const res = await request(app)
        .post('/contacts')
        .send({
          id: 100,
          param: 2000,
        })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Incorrect field (param) in parameters for update/create contact');
    });

    it('should not update contact because empty wrong parameters in body', async () => {
      const res = await request(app)
        .post('/contacts')
        .send({})
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'No parameters passed for update/create contact');
    });
  });

  describe('GET /contacts/{id}', () => {
    it('should return contact data', async () => {
      const res = await request(app)
        .get('/contacts/1')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(200);
      expect(res.body)
        .toHaveProperty('id', 1);
      expect(res.body)
        .toHaveProperty('firstname', 'Иван');
    });

    it('should not found contact', async () => {
      const res = await request(app)
        .get('/contacts/500')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(404);
      expect(res.body)
        .toHaveProperty('error', 'Contact not found');
    });
  });

  describe('PATCH /contacts/{id}', () => {
    it('should return error', async () => {
      const res = await request(app)
        .patch('/contacts/500')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Contact not found');
    });

    it('should not update contact because wrong parameters in body', async () => {
      const res = await request(app)
        .patch('/contacts/1')
        .send({
          id: 100,
          param: 2000,
        })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Incorrect field (param) in parameters for update/create contact');
    });

    it('should update contact', async () => {
      const res = await request(app)
        .patch('/contacts/1')
        .send({
          id: 100,
          firstname: 'test',
        })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(200);
      expect(res.body)
        .toHaveProperty('firstname', 'test');
    });

    it('should not update contact because given incorrect id', async () => {
      const res = await request(app)
        .patch('/contacts/1000')
        .send({
          id: 100,
          firstname: 'test',
        })
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Contact not found');
    });

    it('should not update contact because empty wrong parameters in body', async () => {
      const res = await request(app)
        .patch('/contacts/1')
        .send({})
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'No parameters passed for update/create contact');
    });
  });

  describe('DELETE /contacts/{id}', () => {
    it('should delete contact', async () => {
      const res = await request(app)
        .delete('/contacts/3')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(200);
      expect(res.body)
        .toHaveProperty('success', true);
    });
    it('should not delete contact and return error', async () => {
      const res = await request(app)
        .delete('/contacts/2')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Has a company with this contact. Can\'t be removed');
    });
    it('should not delete contact because incorrect id was given', async () => {
      const res = await request(app)
        .delete('/contacts/200')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode)
        .toEqual(400);
      expect(res.body)
        .toHaveProperty('error', 'Contact not found');
    });
  });

  afterAll(async () => {
    app.close();
  });
});
