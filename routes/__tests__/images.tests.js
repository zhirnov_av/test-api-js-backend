const request = require('supertest');
const app = require('../../app');

describe('images tests', () => {
  let token = '';
  beforeAll(async () => {
    const res = await request(app)
      .post('/auth')
      .send({
        user: 'admin',
        password: 'test',
      });
    const tmp = res.headers.authorization.split(' ')[1];
    token = tmp;
  });

  describe('POST /comapnies/{id}/iamges', () => {
    it('should add image to company and get it', async () => {
      let res = await request(app)
        .post('/companies/1/images')
        .attach('file', './public/0b8fc462dcabf7610a91.png')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('name');
      expect(res.body).toHaveProperty('image');
      expect(res.body).toHaveProperty('thumb');
      expect(res.body).toHaveProperty('id');

      const imageId = res.body.id;

      res = await request(app)
        .get(`/companies/1/images/${imageId}/thumb`)
        .set({ Authorization: `Bearer ${token}` });
      expect(res).toHaveProperty('body');
      expect(Buffer.isBuffer(res.body)).toBeTruthy();
    });

    it('should add image to company and delete it', async () => {
      let res = await request(app)
        .post('/companies/1/images')
        .attach('file', './public/0b8fc462dcabf7610a91.png')
        .set({ Authorization: `Bearer ${token}` });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('name');
      expect(res.body).toHaveProperty('image');
      expect(res.body).toHaveProperty('thumb');
      expect(res.body).toHaveProperty('id');

      const imageId = res.body.id;

      res = await request(app)
        .get('/companies/1')
        .set({ Authorization: `Bearer ${token}` });
      expect(res).toHaveProperty('body');
      expect(res.body).toHaveProperty('photos');
      expect(Array.isArray(res.body.photos)).toBeTruthy();

      const photosLength = res.body.photos.length;

      res = await request(app)
        .delete(`/companies/1/images/${imageId}`)
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(200);

      res = await request(app)
        .get('/companies/1')
        .set({ Authorization: `Bearer ${token}` });
      expect(res).toHaveProperty('body');
      expect(res.body).toHaveProperty('photos');
      expect(Array.isArray(res.body.photos)).toBeTruthy();
      expect(res.body.photos.length).toEqual(photosLength - 1);
    });

    it('should get error because incorrect company id was passed', async () => {
      const res = await request(app)
        .post('/companies/100/images')
        .attach('file', './public/0b8fc462dcabf7610a91.png')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(404);
      expect(res.body).toHaveProperty('error', 'No company with ID 100');
    });

    it('should get error because no file passed', async () => {
      const res = await request(app)
        .post('/companies/100/images')
        .set({ Authorization: `Bearer ${token}` });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toHaveProperty('error', 'No file for upload passed');
    });
  });

  it('should get error because incorrect company id was passed', async () => {
    const res = await request(app)
      .delete('/companies/100/images/1')
      .set({ Authorization: `Bearer ${token}` });
    expect(res.statusCode).toEqual(404);
    expect(res.body).toHaveProperty('error', 'No company with ID 100');
  });

  it('should get error because incorrect image id was passed', async () => {
    const res = await request(app)
      .delete('/companies/1/images/110')
      .set({ Authorization: `Bearer ${token}` });
    expect(res.statusCode).toEqual(404);
    expect(res.body).toHaveProperty('error', 'No image with ID 110 at company with id 1');
  });

  it('should get error because because wrong file type', async () => {
    const res = await request(app)
      .post('/companies/1/images')
      .attach('file', './package.json')
      .set({ Authorization: `Bearer ${token}` });
    expect(res.statusCode).toEqual(400);
    expect(res.body).toHaveProperty('error', 'Only image files are allowed');
  });

  afterAll(async () => {
    app.close();
  });
});
