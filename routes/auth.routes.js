const { Router } = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const logger = require('../services/logger')(module);
const authModel = require('../models/auth.model');

const router = Router();

router.post('/', async (req, res) => {
  const { user, password } = req?.body;

  if (!user) {
    logger.error('No user passed');
    return res.status(400).json({
      error: 'No user passed',
    });
  }

  if (!await authModel.checkUser(user, password)) {
    logger.error('User not  found or incorrect password');
    return res.status(401).json({
      error: 'User not found or incorrect password',
    });
  }

  const token = jwt.sign(
    { user },
    config.app,
    {
      expiresIn: config.jwt_ttl,
    },
  );

  res.header('Authorization', `Bearer ${token}`);
  return res.status(200).end();
});

module.exports = router;
