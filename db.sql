DROP SCHEMA IF EXISTS TEST_API CASCADE;
CREATE SCHEMA IF NOT EXISTS TEST_API;
CREATE EXTENSION IF NOT EXISTS pgcrypto;

create table test_api.accounts
(
	user_id varchar not null
		constraint accounts_pk
			primary key,
	password varchar
);
comment on table test_api.accounts is 'Пользовательские аккаунты';
comment on column test_api.accounts.user_id is 'Идентификатор пользователя';
comment on column test_api.accounts.password is 'Пароль';
alter table test_api.accounts owner to postgres;

create table test_api.companies
(
	id serial not null
		constraint companies_pk
			primary key,
	contact_id integer,
	name varchar,
	short_name varchar,
	business_entity varchar,
	contract_id varchar,
	contract_date date,
	status varchar,
	address varchar,
	created_at timestamp not null,
	updated_at timestamp not null
);

comment on table test_api.companies is 'Компании';

alter table test_api.companies owner to postgres;

insert into TEST_API.accounts(USER_ID, PASSWORD) values(upper('admin'), crypt('test', gen_salt('bf', 8)));

INSERT INTO test_api.companies (contact_id, name, short_name, business_entity, contract_id, contract_date, status, address, created_at, updated_at) VALUES (1, 'ООО Фирма «Перспективные захоронения»', 'Перспективные захоронения', 'ООО', '12345', '2021-03-03', 'active', 'г. Сургут, ул. Ленина, 11', '2021-05-17', '2021-05-17');
INSERT INTO test_api.companies (contact_id, name, short_name, business_entity, contract_id, contract_date, status, address, created_at, updated_at) VALUES (2, 'ОАО Фирма «Рога и копыта»', 'Рога и копыта', 'ОАО', '12345', '2021-03-01', 'active', 'г. Сургут, ул. Мира, 12', '2021-05-17', '2021-05-17');
INSERT INTO test_api.companies (contact_id, name, short_name, business_entity, contract_id, contract_date, status, address, created_at, updated_at) VALUES (4, 'ПАО «Строй сэйл', 'Строй сэйл', 'ПАО', '54321', '2021-05-01', 'active', 'г. Тюмень, ул. Пушкина, 12', '2021-05-17', '2021-05-17');
INSERT INTO test_api.companies (contact_id, name, short_name, business_entity, contract_id, contract_date, status, address, created_at, updated_at) VALUES (5, 'ИП Иванов Иван Иванович', 'Иваныч', 'ИП', 'А98', '2021-04-03', 'active', 'г. Омск, ул. Чехова, 20 - 3', '2021-05-18', '2021-05-18');

create table test_api.images
(
	id serial not null
		constraint images_pk
			primary key,
	company_id int,
	name varchar not null,
	image_data bytea not null,
	thumb_data bytea not null
);
comment on table test_api.images is 'Изображения';
comment on column test_api.images.id is 'Уникальный идентификатор';
comment on column test_api.images.company_id is 'Идентификатор компании';
comment on column test_api.images.name is 'Имя файла';
comment on column test_api.images.image_data is 'Содержимое файла с изображением';
comment on column test_api.images.thumb_data is 'Эскиз';

alter table test_api.images owner to postgres;

create table test_api.company_types_dict
(
	id serial not null
		constraint company_types_dict_pk
			primary key,
	type varchar not null
);
comment on table test_api.company_types_dict is 'Справочник типов компаний';
comment on column test_api.company_types_dict.id is 'Идентификатор';
comment on column test_api.company_types_dict.type is 'Тип';
alter table test_api.company_types_dict owner to postgres;

insert into test_api.company_types_dict(type) values('agent');
insert into test_api.company_types_dict(type) values('contractor');
insert into test_api.company_types_dict(type) values('another type');

create table test_api.company_types
(
	company_id int,
	type_id int,
    primary key (company_id, type_id)
);
comment on table test_api.company_types is 'Типы компаний';
comment on column test_api.company_types.company_id is 'Идентификатор компании';
comment on column test_api.company_types.type_id is 'Идентификатор типа';
alter table test_api.company_types owner to postgres;

insert into test_api.company_types(company_id, type_id) values(1, 1);
insert into test_api.company_types(company_id, type_id) values(1, 2);
insert into test_api.company_types(company_id, type_id) values(1, 3);
insert into test_api.company_types(company_id, type_id) values(2, 1);
insert into test_api.company_types(company_id, type_id) values(3, 2);
insert into test_api.company_types(company_id, type_id) values(3, 3);

create table test_api.contacts
(
	id serial not null
		constraint contacts_pk
			primary key,
	lastname varchar,
	firstname varchar,
	patronymic varchar,
	phone varchar,
	email varchar,
	created_at timestamp,
	updated_at timestamp
);
comment on table test_api.contacts is 'Контакты';
comment on column test_api.contacts.id is 'Уникальный идентификатор';
comment on column test_api.contacts.lastname is 'Фамилия';
comment on column test_api.contacts.firstname is 'Имя';
comment on column test_api.contacts.patronymic is 'Отчество';
comment on column test_api.contacts.phone is 'Телефон';
comment on column test_api.contacts.email is 'email';
comment on column test_api.contacts.created_at is 'Дата создания';
comment on column test_api.contacts.updated_at is 'Дата модификации';

insert into test_api.contacts(lastname, firstname, patronymic, phone, email, created_at, updated_at) values ('Иванов', 'Иван', 'Иванович', '79344562312', 'aaa@gmail.com', '2020-03-01', '2020-03-01');
insert into test_api.contacts(lastname, firstname, patronymic, phone, email, created_at, updated_at) values ('Петров', 'Петр', 'Петрович', '79323212567', 'petrov@yahoo.com', '2020-04-03', '2020-04-03');
insert into test_api.contacts(lastname, firstname, patronymic, phone, email, created_at, updated_at) values ('Сидоров', 'Сергей', 'Васильевич', '79222115217', 'sidorov77@yahoo.com', '2020-04-05', '2020-04-05');
insert into test_api.contacts(lastname, firstname, patronymic, phone, email, created_at, updated_at) values ('Обломов', 'Иван', 'Сергеевич', '79125734976', 'obl@yandex.ru', '2021-02-01', '2021-03-15');
insert into test_api.contacts(lastname, firstname, patronymic, phone, email, created_at, updated_at) values ('Сиников', 'Георгий', 'Дмитриевич', '79222459856', 'grisha@mail.ru', '2020-05-05', '2020-05-05');
