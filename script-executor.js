const fs = require('fs');
const { pool } = require('./services/db');
const logger = require('./services/logger')(module);

const sql = fs.readFileSync('db.sql').toString();

pool.query(sql).then(() => {
  process.exit(0);
}).catch((err) => {
  logger.error(err);
  process.exit(1);
});
