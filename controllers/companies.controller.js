const config = require('../config.json');
const model = require('../models/companies.model');

module.exports = {
  get,
  getAll,
  update,
  create,
  del,
};

/*
const COMPANY = {
  id: config.company_id,
  contactId: config.contact_id,
  name: 'ООО Фирма «Перспективные захоронения»',
  shortName: 'Перспективные захоронения',
  businessEntity: 'ООО',
  contract: {
    no: '12345',
    issue_date: '2015-03-12T00:00:00Z',
  },
  type: ['agent', 'contractor'],
  status: 'active',
  createdAt: '2020-11-21T08:03:00Z',
  updatedAt: '2020-11-23T09:30:00Z',
};
*/

async function get(req, res) {
  const id = req.params?.id;

  try {
    const oResponse = await model.findOne(id);
    _fixImageUrl(req, oResponse, id);
    /*
    oResponse.photos.forEach((item) => {
      item.filepath = `${URL}companies/${id}/images/${item.id}/image`;
      item.thumbpath = `${URL}companies/${id}/images/${item.id}/thumb`;
    });
    */

    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(404).json({ error: e.message });
  }
}

async function getAll(req, res) {
  let sortParams = [];
  if (req.query?.sort) {
    sortParams = req.query.sort instanceof Array ? req.query.sort : [req.query.sort];
  }
  let filterParams = [];
  if (req.query?.filter) {
    filterParams = req.query.filter instanceof Array ? req.query.filter : [req.query.filter];
  }
  const pageOptions = {
    perPage: req.query?.perPage || config.perPage,
    page: req.query?.page || 1,
  };

  try {
    const oResponse = await model.findAll(sortParams, filterParams, pageOptions);
    oResponse.items.forEach((company) => {
      _fixImageUrl(req, company);
      /*
      company.photos.forEach((item) => {
        item.filepath = `${URL}companies/${company.id}/images/${item.id}/image`;
        item.thumbpath = `${URL}companies/${company.id}/images/${item.id}/thumb`;
      });
      */
    });

    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

async function update(req, res) {
  const id = req.params?.id;
  try {
    const updatedId = await model.update(id, req.body);
    const oResponse = await model.findOne(updatedId);
    _fixImageUrl(req, oResponse);
    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

async function del(req, res) {
  const id = req.params?.id;
  try {
    await model.delete(id);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
  return res.status(200).end();
}

async function create(req, res) {
  try {
    const id = await model.create(req.body);

    const oResponse = await model.findOne(id);
    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

function _fixImageUrl(req, modelResponse) {
  const URL = _getCurrentURL(req);
  modelResponse.photos.forEach((item) => {
    item.filepath = `${URL}companies/${modelResponse.id}/images/${item.id}/image`;
    item.thumbpath = `${URL}companies/${modelResponse.id}/images/${item.id}/thumb`;
  });
}

function _getCurrentURL(req) {
  const { port } = config;
  return `${req.protocol}://${req.hostname}${port === '80' || port === '443' ? '' : `:${port}`}/`;
}
