/* eslint-disable no-ex-assign */
const crypto = require('crypto');
const fs = require('fs');
const jimp = require('jimp');
const path = require('path');
const companiesModel = require('../models/companies.model');
const config = require('../config.json');
const logger = require('../services/logger')(module);

const thumbSize = config.thumb_size;

module.exports = {
  saveImage,
  removeImage,
  getImage,
  getThumb,
};

async function saveImage(req, res) {
  try {
    logger.info('File upload started');
    const file = req.files.file[0];
    const companyId = req.params.id;
    const { user } = req;

    const fileExtension = path.extname(file.originalname).toLowerCase();
    const fileName = crypto.randomBytes(10).toString('hex');

    const uploadedFileName = fileName + fileExtension;
    const uploadedFileThumbName = `${fileName}_${thumbSize}x${thumbSize}${fileExtension}`;

    const tempFilePath = file.path;

    const targetFilePath = path.resolve(`${config.static_dir}${config.images_dir}${user}/${uploadedFileName}`);
    const targetThumbPath = path.resolve(`${config.static_dir}${config.images_dir}${user}/${uploadedFileThumbName}`);

    await _resize(tempFilePath, targetThumbPath)
      .catch((err) => {
        res.status(500);
        throw err;
      });

    await _rename(tempFilePath, targetFilePath)
      .catch((err) => {
        res.status(500);
        throw err;
      });

    let imageData = await fs.readFileSync(targetFilePath, { encoding: 'hex' });
    imageData = `\\x${imageData}`;

    let thumbData = await fs.readFileSync(targetThumbPath, { encoding: 'hex' });
    thumbData = `\\x${thumbData}`;

    const imageId = await companiesModel.addImage(companyId, uploadedFileName, imageData, thumbData)
      .catch((err) => {
        res.status(500);
        throw err;
      });
    await _remove(targetFilePath).catch((err) => { logger.error(err); });
    await _remove(targetThumbPath).catch((err) => { logger.error(err); });

    logger.info('File upload successfully finished');

    return res.status(200).json({
      id: imageId,
      name: uploadedFileName,
      image: `${_getCurrentURL(req)}companies/${companyId}/images/${imageId}/image`,
      thumb: `${_getCurrentURL(req)}companies/${companyId}/images/${imageId}/thumb`,
    });
  } catch (error) {
    logger.error(error);
    return res.json({ error });
  }
}

async function getImage(req, res) {
  const { imageId } = req.params;

  try {
    const fileData = await companiesModel.getImage(imageId);
    res.type(fileData.extension);
    return res.send(Buffer.from(fileData.data, 'binary'));
  } catch (e) {
    return res.status(404).json({ error: e.message });
  }
}

async function getThumb(req, res) {
  const { imageId } = req.params;

  try {
    const fileData = await companiesModel.getImage(imageId, true);
    res.type(fileData.extension);
    return res.send(Buffer.from(fileData.data, 'binary'));
  } catch (e) {
    return res.status(404).json({ error: e.message });
  }
}

async function removeImage(req, res) {
  try {
    await companiesModel.removeImage(req.params.imageId);

    return res.status(200).end();
  } catch (error) {
    logger.error(error);
    return res.json({ error });
  }
}

async function _rename(temp, target) {
  return new Promise((resolve, reject) => {
    fs.rename(temp, target, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
}

async function _resize(file, filePath) {
  const image = await jimp.read(file);
  await image.resize(jimp.AUTO, 180);
  const w = image.bitmap.width;
  const h = image.bitmap.height;
  await image.crop((w - thumbSize) / 2, (h - thumbSize) / 2, thumbSize, thumbSize);
  await image.writeAsync(filePath);
}

async function _remove(file) {
  return new Promise((resolve, reject) => {
    fs.unlink(file, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
}

function _getCurrentURL(req) {
  const { port } = config;
  return `${req.protocol}://${req.hostname}${port === '80' || port === '443' ? '' : `:${port}`}/`;
}
