const model = require('../models/contacts.model');

module.exports = {
  get,
  update,
  del,
  create,
};

async function get(req, res) {
  const id = req.params?.id;

  try {
    const oResponse = await model.get(id);

    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(404).json({ error: e.message });
  }
}

async function update(req, res) {
  try {
    const oResponse = await model.update(req.params.id, req.body);
    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

async function create(req, res) {
  try {
    const id = await model.create(req.body);

    const oResponse = await model.get(id);
    return res.status(200).json(oResponse);
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}

async function del(req, res) {
  try {
    const rowCount = await model.del(req.params.id);
    return rowCount
      ? res.status(200).json({ success: true })
      : res.status(404).json({ error: 'Contact not found' });
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
}
