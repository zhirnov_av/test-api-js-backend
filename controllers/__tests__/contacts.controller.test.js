const contactsController = require('../contacts.controller');

describe('contacts.controller tests', () => {
  it('should return 404 error', async () => {
    const req = { params: { id: '500' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
      json: jest.fn().mockReturnThis(),
    };
    await contactsController.get(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toBeCalledWith({ error: 'Contact not found' });
  });

  it('should return 404 error then try to delete not existed contact', async () => {
    const req = { params: { id: '500' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
      json: jest.fn().mockReturnThis(),
    };
    await contactsController.del(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toBeCalledWith({ error: 'Contact not found' });
  });

  it('should correct update record', async () => {
    const RealDate = Date;
    global.Date = class extends RealDate {
      constructor() {
        super();
        return new RealDate('2021-05-21T19:00:00.000Z');
      }
    };
    const contactRecord = {
      id: 1,
      firstname: 'Иван',
      lastname: 'Иванов',
      patronymic: 'Иванович',
      phone: '77777777777',
      email: 'aaa@gmail.com',
      createdAt: new Date('2020-02-29T19:00:00.000Z'),
      updatedAt: new Date('2020-05-21T19:00:00.000Z'),
    };
    const req = { params: { id: '1' }, body: contactRecord };
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
      json: jest.fn().mockReturnThis(),
    };
    const next = jest.fn();
    try {
      await contactsController.update(req, res, next);
    } finally {
      global.Date = RealDate;
    }
    expect(res.status).toBeCalledWith(200);
    expect(res.json).toBeCalledWith(contactRecord);
  });
});
