const companiesController = require('../companies.controller');

describe('contacts.controller tests', () => {
  it('get full list of clients', async () => {
    const req = {};
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
      json: jest.fn().mockReturnThis(),
    };
    await companiesController.getAll(req, res);
    expect(res.status).toBeCalledWith(200);
  });
  it('get one clients', async () => {
    const req = { params: { id: 2 } };
    const res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
      json: jest.fn().mockReturnThis(),
    };
    await companiesController.getAll(req, res);
    expect(res.status).toBeCalledWith(200);
  });
});
