const { Pool } = require('pg');
const config = require('../config.json');
const logger = require('./logger')(module);

const options = config.db.connection;
options.host = process.env.DB_HOST || 'localhost';
logger.info(`DB_HOST variable is ${process.env.DB_HOST}`);
logger.info(`DB host is ${options.host}`);
const pool = new Pool(options);

module.exports.pool = pool;
module.exports.SCHEMA_NAME = config.db.schema;

module.exports.startTr = async () => {
  const client = await pool.connect();
  await client.query('create temporary table if not exists temp_id_table(id int) on commit delete rows');
  await client.query('BEGIN');
  return client;
};

module.exports.commitTr = async (client) => {
  await client.query('COMMIT');
  client.release();
};

module.exports.rollbackTr = async (client) => {
  await client.query('ROLLBACK');
  client.release();
};
