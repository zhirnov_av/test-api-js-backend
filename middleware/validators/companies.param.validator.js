const model = require('../../models/companies.model');
const modelContacts = require('../../models/contacts.model');

module.exports = {
  getAllCompanies,
  del,
  create,
  update,
};

async function getAllCompanies(req, res, next) {
  try {
    _checkSortParams(req.query.sort);
    _checkFilterParams(req.query.filter);
    _checkPageParams(req.query?.page, req.query?.perPage);
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }

  return next();
}

async function del(req, res, next) {
  try {
    const { id } = req.params;
    // если exception не случится, значит все нормально, такая компания существует
    await model.findOne(id);
    // думаю удалять связанный контакт не нужно...
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }
  return next();
}

async function create(req, res, next) {
  try {
    const company = req?.body;
    if (company.contactId) {
      // если не найдется, то будет сгенерирован exception
      await modelContacts.get(company.contactId);
    }
    if (company.type) await _checkCompanyTypes(company.type);
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }
  return next();
}

async function update(req, res, next) {
  try {
    const id = req.params?.id;
    await model.findOne(id);

    const company = req.body;
    if (company.contactId) {
      // если не найдется, то будет сгенерирован exception
      await modelContacts.get(company.contactId);
    }

    // проверим список полей в переданном объекте
    const objectKeys = Object.keys(req?.body);
    if (objectKeys.length === 0) {
      res.status(400);
      throw new Error('No parameters passed for company update');
    }
    const updatableFields = model.COMPANY_FIELDS.filter((item) => item.updateOrCreate);
    const correctKeysInObject = [];
    updatableFields.forEach((item) => {
      const foundKey = objectKeys.find((key) => key === item.apiField);
      if (foundKey) correctKeysInObject.push(foundKey);
    });
    if (correctKeysInObject.length === 0) {
      res.status(400);
      throw new Error('No parameters passed for company update');
    }

    objectKeys.forEach((key) => {
      if (!model.COMPANY_FIELDS.find((field) => field.apiField === key)) {
        throw new Error(`Incorrect field (${key}) in parameters for update company`);
      }
    });

    if (company.type) await _checkCompanyTypes(company.type);
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }
  return next();
}

const _checkCompanyTypes = async (types) => {
  const sTypesError = 'Given company\'s types are not correct';
  if (!Array.isArray(types)) throw new Error(sTypesError);
  if (!await model.checkCompanyTypes(types)) throw new Error(sTypesError);
};

const _checkSortParams = (sortParams) => {
  if (!sortParams) return;
  const sortArray = sortParams instanceof Array ? sortParams : [sortParams];
  sortArray.forEach((item) => {
    if (!/^[a-zA-Z]+(.asc)?(.desc)?$/g.test(item)) throw new Error('Not correct sort params');
    const fieldName = item.split('.')[0];
    const sortFields = model.COMPANY_FIELDS
      .filter((field) => field.canSortUsage)
      .map((field) => field.apiField.toUpperCase());
    if (sortFields.indexOf(fieldName.toUpperCase()) === -1) throw new Error(`Field ${fieldName} does not supported for sorting`);
  });
};

const _checkFilterParams = (filterParams) => {
  if (!filterParams) return;
  const aFilters = filterParams instanceof Array ? filterParams : [filterParams];
  aFilters.forEach((item) => {
    if (!/^[a-zA-Z]+\(([a-zA-Z0-9А-Яа-я]+)(,[a-zA-Z0-9А-Яа-я]+)*\)$/g.test(item)) {
      throw new Error('Not correct filter params');
    }
    const aGroups = /^([a-zA-Z]+)\(.*\)$/g.exec(item);

    const fieldName = aGroups[1];

    const filterFields = model.COMPANY_FIELDS
      .filter((field) => field.canFilterUsage)
      .map((field) => field.apiField.toUpperCase());
    if (filterFields.indexOf(fieldName.toUpperCase()) === -1) throw new Error(`Field ${fieldName} does not supported for filtering`);
  });
};

const _checkPageParams = (page, perPage) => {
  if (!page && !perPage) return;
  const iPage = +page;
  const iPerPage = +perPage;

  if (page && (iPage < 0 || Number.isNaN(iPage))) throw new Error('page parameter must be greater then or equal to 0');
  if (perPage && (iPerPage < 0 || Number.isNaN(iPerPage))) throw new Error('perPage parameter must be greater then or equal to 0');
};
