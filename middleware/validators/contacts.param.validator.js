const { CONTACT_FIELDS, get } = require('../../models/contacts.model');
const { getCompanyIdByContactId } = require('../../models/companies.model');

module.exports.update = async (req, res, next) => {
  try {
    // проверим, а есть ли такой контакт
    await get(req.params?.id);
    // проверим список полей в переданном объекте
    /*
    const objectKeys = Object.keys(req?.body);
    if (objectKeys.length === 0) {
      res.status(400);
      throw new Error('No parameters passed contact update');
    }
    objectKeys.forEach((key) => {
      if (!CONTACT_FIELDS.find((field) => field.apiField === key)) {
        throw new Error(`Incorrect field (${key}) in parameters for update contact`);
      }
    });
    */
    _checkObjectForFields(req?.body);
    // далее можно regexp'ом проверить еще валидность email и номера телефона
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }
  return next();
};

module.exports.create = async (req, res, next) => {
  try {
    // проверим список полей в переданном объекте
    _checkObjectForFields(req?.body);
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }
  return next();
};

module.exports.deleteContact = async (req, res, next) => {
  try {
    // проверим, а есть ли такой контакт
    await get(req.params.id);
    // проверим а есть ли компания с указанным контактом, если есть - удалять нельзя
    const companyId = await getCompanyIdByContactId(req.params.id);
    if (companyId) {
      throw new Error('Has a company with this contact. Can\'t be removed');
    }
  } catch (e) {
    res.status(400);
    return res.json({ error: e.message });
  }
  return next();
};

function _checkObjectForFields(object) {
  const objectKeys = Object.keys(object);
  if (objectKeys.length === 0) {
    throw new Error('No parameters passed for update/create contact');
  }
  objectKeys.forEach((key) => {
    if (!CONTACT_FIELDS.find((field) => field.apiField === key)) {
      throw new Error(`Incorrect field (${key}) in parameters for update/create contact`);
    }
  });
}
