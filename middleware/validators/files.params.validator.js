const fs = require('fs');
const path = require('path');
const companiesModel = require('../../models/companies.model');

const logger = require('../../services/logger')(module);

module.exports = {
  addCompanyImage,
  removeCompanyImage,
};

async function addCompanyImage(req, res, next) {
  try {
    if (!req?.files?.file?.[0]) {
      res.status(400);
      throw new Error('No file for upload passed');
    }

    try {
      await companiesModel.findOne(req.params.id);
    } catch (e) {
      res.status(404);
      throw new Error(`No company with ID ${req.params.id}`);
    }

    const file = req.files.file[0];
    const fileExtension = path.extname(file.originalname).toLowerCase();
    const tempFilePath = file.path;

    // if (!(fileExtension === '.png'
    //     || fileExtension === '.jpg' || fileExtension === '.jpeg' || fileExtension === '.gif')) {
    //
    // Мне кажется так компактнее и не в ущерб читаемости
    if (['.png', '.jpg', '.jpeg', '.gif'].indexOf(fileExtension) === -1) {
      _remove(tempFilePath).catch((err) => { logger.error(err); });
      res.status(400);
      throw new Error('Only image files are allowed');
    }
  } catch (error) {
    logger.error(error);
    return res.json({ error: error.message });
  }

  return next();
}

async function removeCompanyImage(req, res, next) {
  try {
    let company;
    try {
      company = await companiesModel.findOne(req.params.companyId);
    } catch (e) {
      res.status(404);
      throw new Error(`No company with ID ${req.params.companyId}`);
    }
    const foundPhoto = company.photos.find((photo) => photo.id === +req.params.imageId);
    if (!foundPhoto) {
      res.status(404);
      throw new Error(`No image with ID ${req.params.imageId} at company with id ${company.id}`);
    }
  } catch (error) {
    logger.error(error);
    return res.json({ error: error.message });
  }

  return next();
}

async function _remove(file) {
  return new Promise((resolve, reject) => {
    fs.unlink(file, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });
}
